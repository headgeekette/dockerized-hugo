FROM ubuntu:20.04 

RUN apt-get update && apt-get install -y \
    wget \
    git \
 && rm -rf /var/lib/apt/lists/*

ENV HUGO_VERSION 0.75.1

RUN wget --no-check-certificate -O hugo.deb https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.deb \
 	&& dpkg -i hugo.deb \
 	&& rm hugo.deb

